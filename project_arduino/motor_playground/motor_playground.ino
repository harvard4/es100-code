#include "MyActuator.h"
#include "mcp2515.h"

RMDMotor motor(0x01, CAN_1000KBPS, MCP_8MHZ);
volatile bool msg_received = false;

/*
 * This lightweight interrupt handler just sets the msgReceived flag
 * which is later checked in the main loop.
 */
void irqHandler() {
  msg_received = true;
}

void setup() {
  // Set RESETB pin on MCP2515 to high (default/normal state; LOW is reset).
  int rstb_can = 5;
  pinMode(rstb_can, OUTPUT);
  digitalWrite(rstb_can, 1);

  /*
   * We wait 0.1s on startup before doing anything. This should give everything more than enough time to reset,
   * allow voltages to stabilize, and so on.
   */
  delay(100);

  // Initialize the various objects that we will use.
  Serial.begin(9600);
  motor.begin();
  Serial.println("Init successful");
  MCP2515::ERROR err = motor.setCANMode(CAN_NORMAL);
  // INTB interrupt occurs on Pin 3.
  attachInterrupt(digitalPinToInterrupt(3), irqHandler, FALLING);
}

void loop() {
  /*
   * Now, handle the interrupt at the start of each loop cycle.
   */
  if (msg_received){
    msg_received = false;
    motor.readMessages();
  }
  struct motorAngle test;
  test.angle ;
  test.max_speed = 420;
  motor.setAngleTargetMT(&test);

  struct motorAngle meas;
  motor.getAngleMT(&meas);
  Serial.println(meas.angle);
  Serial.println("");
  delay(1000);
}

#include "AMT203.h"

AMT203 amt203(10);

void setup() {
  Serial.begin(115200);
  amt203.begin();
  Serial.println("----------Initialization successful.----------");
  for(int i=0; i<15; i++) {
    Serial.print("Zero-point calibration in ...... ");
    Serial.println(15 - i);
    delay(1000);
  }
  amt203.setZero();
  Serial.println("Zeroing was successful.");
  delay(100);
}

void loop() {
  double angle = amt203.getPosition();
  Serial.print("Sense Encoder Angle: ");
  Serial.println(angle);
  delay(1000);
}

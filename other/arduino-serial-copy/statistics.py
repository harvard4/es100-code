import csv
import numpy as np
import os
import matplotlib.pyplot as plt

def isFloat(string):
    try:
        float(string)
        return True
    except ValueError:
        return False

'''
This method trims and processes the data, making it easy to plot down the road.
'''
def trimFiles(subfolder):
    for filename in os.listdir(f'./logs/{subfolder}', ):
        if filename.endswith('.csv'):
            with open(os.path.join(f"./logs/{subfolder}", filename)) as readfile:
                try:
                    os.mkdir(f"./logs/{subfolder}/trimmed")
                except FileExistsError:
                    pass # If this folder already exists we don't need to make it

                with open(os.path.join(f"./logs/{subfolder}/trimmed", filename), 'w', newline="") as writefile:
                    reader = csv.reader(readfile)
                    writer = csv.writer(writefile)
                    starting_millis = 0

                    for idx, row in enumerate(reader):
                        # Always copy over the header row with column names
                        if row[0] == "Data Index":
                            row.append("Error")
                            writer.writerow(row)

                        row = [item for item in row if isFloat(item)]

                        """
                        Ignore the first few rows which are countdown text, as well as incomplete rows. Measurement [0]
                        is always invalid because it hasn't received data yet from the motor.
                        """
                        if len(row) == 4:
                            if int(row[0]) == 0:
                                # Ignore the datapoint in general but use the millis() value as t=0.
                                starting_millis = int(row[1])
                            else:
                                """
                                If both commanded position and measured position are within 0.05 of -30, then the
                                datapoint represents stationary downtime at the beginning/end of each experiment and
                                should be removed.
                                """
                                if abs(-30 - float(row[2])) > 0.05 and abs(-30 - float(row[3])) > 0.05:
                                    row[1] = str(int(row[1]) - starting_millis)
                                    row[2] = str(-1 * float(row[2]))
                                    row[3] = str(-1 * float(row[3]))
                                    # Error is defined as (measured - commanded)
                                    row.append(str(float(row[3]) - float(row[2])))
                                    writer.writerow(row)

'''
This function plots actual position vs. time for all tests of a particular type.
'''
def plotErrorvsTime(subfolder):
    plt.figure()
    filenum = 0
    errors = []
    x_axes = []
    control_values = []
    for filename in os.listdir(f'./logs/{subfolder}/trimmed', ):
        if filename.endswith('.csv'):
            x_axes.append([])
            errors.append([])
            control_values.append([])
            with open(os.path.join(f"./logs/{subfolder}/trimmed", filename)) as readfile:
                reader = csv.reader(readfile)
                t_offset = 0
                for idx, row in enumerate(reader):
                    if idx == 1:
                        t_offset = float(row[1])
                    if isFloat(row[0]):
                        control_values[filenum].append(float(row[2]))
                        x_axes[filenum].append(0.001 * (float(row[1]) - t_offset))
                        errors[filenum].append(float(row[4]))
        filenum += 1

    sequenceLengths = []

    for error_sequence in errors:
        sequenceLengths.append(len(error_sequence))

    avg_times = x_axes[sequenceLengths.index(max(sequenceLengths))]
    avg_values = []
    avg_controls = []

    # Iterating through "time" here
    for i in range(max(sequenceLengths)):
        num_errors = 0
        err_sum = 0
        num_values = 0
        value_sum = 0
        # 20 experiments
        for j in range(20):
            # If the current "time" is defined for experiment j then add it to the average
            if i < sequenceLengths[j]:
                err_sum += errors[j][i]
                num_errors += 1
                value_sum += control_values[j][i]
                num_values += 1
        avg_values.append(err_sum / num_errors)
        avg_controls.append(value_sum / num_values)
    '''
    Calculate velocities
    '''
    avg_speeds = np.diff(avg_controls) / np.diff(avg_times)
    # Now, we have to smooth this noisy data
    window_size = 50
    # For some reason the simple data is way more noisy
    if subfolder == 'simple_movement':
        window_size = 150
    window = np.ones(window_size) / window_size # Uniform convolution window
    avg_speeds = list(np.convolve(avg_speeds, window, mode='same'))
    avg_speeds.append(avg_speeds[-1]) # Repeat the last value to make it line up with time
    '''
    Calculate accelerations
    '''
    avg_accels = np.diff(avg_speeds) / np.diff(avg_times)
    # Now, we have to smooth this noisy data
    window_size = 50
    # For some reason the simple data is way more noisy
    if subfolder == 'simple_movement':
        window_size = 150
    window = np.ones(window_size) / window_size  # uniform convolution window
    avg_accels = list(np.convolve(avg_accels, window, mode='same'))
    avg_accels.append(avg_accels[-1]) # Repeat the last value to make it line up with time

    while len(avg_accels) < len(avg_times):
        avg_accels.append(avg_accels[-1])

    fig, ax1 = plt.subplots()
    ax2 = ax1.twinx()
    line1, = ax1.plot(avg_times, avg_values, color='darkorange', label="Error (deg)")
    line2, = ax2.plot(avg_times, avg_accels, color='royalblue', label="Acceleration (d/s^2)")
    ax2.set_ylim(-500, 500)
    ax1.set_xlabel("Time (s)")
    ax1.set_ylabel("Average Error (deg)")
    ax2.set_ylabel("Angular acceleration (deg/s^2)")

    plt.title("System Error as a Function of Time")
    lines = [line1, line2]
    labels = [l.get_label() for l in lines]
    plt.legend(lines, labels, loc='upper left')

    plt.tight_layout()
    plt.savefig(os.path.join(f"./logs/{subfolder}", f"{subfolder}_error_v_time.png"), dpi=1200, bbox_inches='tight', pad_inches=0.5)
    plt.show()

def plotTrajectories(subfolder):
    plt.figure()

    for filename in os.listdir(f'./logs/{subfolder}/trimmed', ):
        if filename.endswith('.csv'):
            x_axis = []
            y_axis = []
            with open(os.path.join(f"./logs/{subfolder}/trimmed", filename)) as readfile:
                reader = csv.reader(readfile)
                t_offset = 0
                for idx, row in enumerate(reader):
                    if idx == 1:
                        t_offset = float(row[1])
                    if isFloat(row[0]):
                        x_axis.append(0.001 * (float(row[1]) - t_offset))
                        y_axis.append(float(row[3]))
            plt.plot(x_axis, y_axis)

            plt.title("Load Arm Trajectories")
            plt.xlabel("Time (s)")
            plt.ylabel("Arm Angle (deg)")

    plt.savefig(os.path.join(f"./logs/{subfolder}", f"{subfolder}_paths.png"), dpi=1200)
    plt.show()

def plotErrorHistogram(subfolder):
    plt.figure()
    errors_all = []
    errors_failed = []
    for filename in os.listdir(f'./logs/{subfolder}/trimmed', ):
        if filename.endswith('.csv'):
            with open(os.path.join(f"./logs/{subfolder}/trimmed", filename)) as readfile:
                reader = csv.reader(readfile)
                for idx, row in enumerate(reader):
                    if isFloat(row[1]):
                        errors_all.append(float(row[4]))
                        if abs(float(row[4])) > 3.5:
                            errors_failed.append(float(row[4]))
    plt.hist(errors_all, bins=50)

    plt.title("Error Histogram")
    plt.xlabel("Error (deg)")
    plt.ylabel("Frequency")

    print(subfolder)
    print((1 - len(errors_failed)/len(errors_all)) * 100.0)
    print(np.std(errors_all))
    print(np.mean(errors_all))
    plt.savefig(os.path.join(f"./logs/{subfolder}", f"{subfolder}_hist.png"), dpi=1200)
    plt.show()



if __name__ == "__main__":
    for folder in os.listdir("./logs/"):
        trimFiles(folder)
        plotTrajectories(folder)
        plotErrorvsTime(folder)
        plotErrorHistogram(folder)


#include "mcp2515.h"
#include "AMT203.h"
#include "MyActuator.h"

RMDMotor motor(0x01, CAN_250KBPS, MCP_8MHZ);
AMT203 amt203(10);
double sense_angle = 0;
const uint16_t high_speed = 420;
const uint16_t low_speed = 50;
uint16_t loop_counter = 0;

/*
 * This lightweight interrupt handler just sets the msgReceived flag
 * which is later checked in the main loop.
 */
void irqHandler() {
  motor.messageReceivedAlert();
}

void setup() {
  // Initialize the various objects that we will use.
  Serial.begin(115200);

  /*
   * 5-second delay before strating anything. We want to make sure everything is powered up
   * before the arduino sends anything over. It seems the motor gets confused if it gets half messages too
   * quickly and it ruins the zeroing.
   */
  for (int i = 0; i < 5; i++){
    Serial.print("System starting in ...... ");
    Serial.println(5 - i);
    delay(1000);
  }
  motor.begin();
  amt203.begin();
  // INTB interrupt occurs on Pin 3.
  attachInterrupt(digitalPinToInterrupt(3), irqHandler, FALLING);
  // Give everything a second -- ICs need to go through startup sequence, voltages need to stabilize, etc.
  delay(1000);
  motor.setCANMode(CAN_NORMAL);
  
  /*
   * If the position acceleration is set to zero, the motor enters direct
   * PID tracking mode. The 10k value for speed acceleration and deceleration
   * are default values from the Assistant software.
   */
  motorAcceleration accs;
  accs.pos_accel = 0;
  accs.pos_decel = 0;
  accs.spd_accel = 10000;
  accs.spd_decel = 10000;

  motor.setAcceleration(&accs);

  Serial.println("----------Initialization successful.----------");

  Serial.println("Data Index, Time (ms), Commanded Value,Measured Value");
  
}

/*
 * Zero for all encoders is defined as LOAD ARM PERPENDICULAR TO GROUND 
 * (i.e., hanging just by gravity)
 */
void loop() {
  Serial.print(loop_counter);
  Serial.print(",");
  Serial.print(millis());
  Serial.print(",");
  sense_angle = amt203.getPosition();

  /*
   * Motor angle ranges from -30 (full extension) to -180 (full contraction).
   * Values outside of this range are meaningless and/or could cause injury to
   * user or machine. The arm should move slowly when it is getting over to the
   * starting balues, and at full speed once it can move fluidly. Fast aggressive
   * movements can break stuff.
   */
  struct motorAngle commandAngle;

  if(-30 < sense_angle && sense_angle < 90){
    sense_angle = -30;
    commandAngle.max_speed = low_speed;
  }
  else if(90 < sense_angle && sense_angle < 180){// Once the encoder reaches -180 it loops back to +180
    sense_angle = -179;
    commandAngle.max_speed = low_speed;
  }
  else {
    commandAngle.max_speed = high_speed;
  }
  commandAngle.angle = sense_angle;
  
  // Command the motor with the new input from the sense encoder
  motor.setAngleTargetMT(&commandAngle);
  Serial.print(sense_angle);
  Serial.print(",");

  
  // Use the motor's own encoder to get live telemetry on what is going on.
  struct motorAngle measAngle;
  motor.getAngleMT(&measAngle);
  Serial.println(measAngle.angle);

  loop_counter++;
}

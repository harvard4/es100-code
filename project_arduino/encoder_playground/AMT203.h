/*
 * AMT203.h is a library which allows an Arduino to interface with the AMT203
 * rotary encoder from CUI devices.
 */
#ifndef AMT203_H
#define AMT203_H

#include <SPI.h>

class AMT203{
    public:
        AMT203(int chip_select);
        double getPosition();
        void setZero();
        void begin();

    private:
        SPIClass *_SPIn;
        double _angle;
        int _cs;
        uint8_t _sendCommand(uint8_t command);
        void _openSPI();
        void _closeSPI();
};

#endif //AMT203_H

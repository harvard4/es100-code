/*
* MyActuator.h is a library for controlling the MyActuator RMD X6 S2 motor over CAN bus, as well as other
* MyActuator products with the same motor controller.
*/

#ifndef MyActuator_h
#define MyActuator_h

#include "Arduino.h"
#include "mcp2515.h"
#include "RingBuf.h"

enum CAN_MODE {
    CAN_NORMAL,
    CAN_LOOPBACK,
    CAN_LISTEN_ONLY,
};

enum LOOP_MODE {
    CURRENT_CONTROL,
    SPEED_CONTROL,
    POSITION_CONTROL
};

// Go to the lab and get these values. 1 is just a placeholder!
static const double CURRENT_KP_MAX = 0.5;
static const double CURRENT_KI_MAX = 0.05;
static const double SPEED_KP_MAX = 0.01;
static const double SPEED_KI_MAX = 0.0005;
static const double POS_KP_MAX = 0.1;
static const double POS_KI_MAX = 0.005;

struct motorPIDs{
  double current_kp, current_ki, speed_kp, speed_ki, pos_kp, pos_ki;
};

/*
 * Units of degrees per second squared (d/s^2)
 */
struct motorAcceleration{
  uint32_t pos_accel, pos_decel, spd_accel, spd_decel;
};

struct motorAngle{
  uint16_t max_speed;
  double angle;
};

/*
*We use MULTI-TURN ANGLE FOR EVERYTHING. When zeroed , it will behave pretty
*much like single-turn angle except 1. It has positive and negative values
*(helpful); 2. It allows for direct PID control mode without acceleration.
*/
class RMDMotor
{
  public:
    RMDMotor(uint8_t id_offset, CAN_SPEED can_speed, CAN_CLOCK can_clock);
    MCP2515::ERROR begin();
    MCP2515::ERROR setCANMode(CAN_MODE can_mode);
    CAN_MODE getCANMode();
    /*
    These methods for writing registers are private. Either find another way to configure interrupts or deal with this later.
    */
    //void readRegisterMCP(MCP2515::REGISTER register);
    //void writeRegisterMCP(MCP2515::REGISTER register, uint8_t value);
    // High-level methods
    MCP2515::ERROR getPID(struct motorPIDs *motorPIDs); //works
    MCP2515::ERROR setPID(struct motorPIDs *motorPIDs, bool temp);
    MCP2515::ERROR getAcceleration(struct motorAcceleration *motorAccel); //works
    MCP2515::ERROR setAcceleration(struct motorAcceleration *motorAccel);
    MCP2515::ERROR getOperatingMode(LOOP_MODE *loop_mode);
    // Low-level methods
    MCP2515::ERROR getAngleMT(struct motorAngle *motorAngle);
    MCP2515::ERROR shutdown();
    MCP2515::ERROR stop();
    MCP2515::ERROR setAngleTargetMT(struct motorAngle *motorAngle);
    MCP2515::ERROR setAngleDeltaTargetMT(struct motorAngle *motorAngle); 
    MCP2515::ERROR reset();
    MCP2515::ERROR brakeRelease();
    MCP2515::ERROR brakeLock();
    void messageReceivedAlert();
  private:
    MCP2515 _mcp2515;
    bool _msg_received = false;
    RingBuf<can_frame *, 16> _rxBuf;
    uint32_t _can_id;
    uint8_t _can_dlc = 8;
    motorPIDs _motorPIDs;
    motorAcceleration _motorAcceleration;
    motorAngle _motorAngle;
    CAN_SPEED _can_speed;
    CAN_CLOCK _can_clock;
    CAN_MODE _can_mode;
    LOOP_MODE _loop_mode;
    MCP2515::ERROR _readMessages();
    MCP2515::ERROR _readMotorMessage(MCP2515::RXBn rxbn);
    MCP2515::ERROR _sendMotorMessage(uint8_t *data);
    void _printCANFrameToSerial(struct can_frame *frame);
};

#endif
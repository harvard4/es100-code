import serial
import time

port = "/dev/cu.usbserial-B0004HZV" # Serial port that arduino is on
num_test = 20
arduino = serial.Serial(port, 115200, timeout=0.01)

if __name__ == "__main__":
    startTime = time.time()
    arduino.flush()

    with open(f"./logs/lurch/log{num_test}.csv", "a") as file:
        # Each test takes 5 seconds: 3 seconds of countdown plus 2 seconds of data
        while time.time() < startTime + 12:
            line = arduino.readline().decode()
            file.write(line)
            print(line, end="")


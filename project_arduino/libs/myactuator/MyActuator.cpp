#include "Arduino.h"
#include "MyActuator.h"
#include "mcp2515.h"
#include <math.h>

;
/*
 * RMDMotor object constructor. The mcp2515.h library as written will generate interrupts on the INTB pin, but not
 * on the RXnBUFB pins or TXnRTSB pins. We have to check the CANINTF buffer to figure out which receive buffer caused
 * the interrupt.
 */
RMDMotor::RMDMotor(uint8_t id_offset, CAN_SPEED can_speed, CAN_CLOCK can_clock) {
  _can_id = 0x140 + id_offset;
  _can_speed = can_speed;
  _can_clock = can_clock;
  // PID Values
  _motorPIDs.current_kp = 0;
  _motorPIDs.current_ki = 0;
  _motorPIDs.pos_kp = 0;
  _motorPIDs.pos_ki = 0;
  _motorPIDs.speed_kp = 0;
  _motorPIDs.speed_ki = 0;
  // Angle values
  _motorAngle.angle = 0;
  _motorAngle.max_speed = 0;
  // Acceleration values
  _motorAcceleration.pos_accel = 0;
  _motorAcceleration.pos_decel = 0;
  _motorAcceleration.spd_accel = 0;
  _motorAcceleration.spd_decel = 0;
}

/*
Things that actually involve physical hardware (rather than just allocating memory, defining variables)
have to occur after the arduino initializes. The init() method is called behind the scenes as part of the
Arduino IDE. TLDR: You can only do hardware-related stuff in setup() or loop().
*/
MCP2515::ERROR RMDMotor::begin() {
  // Set RESETB pin on MCP2515 to high (default/normal state; LOW is reset).
  int rstb_can = 5;
  pinMode(rstb_can, OUTPUT);
  digitalWrite(rstb_can, 1);
  
  _mcp2515 = MCP2515(9);  // This line uses the meaningless default constructor to create an empty instance of MCP2515 object.
  MCP2515::ERROR err0 = _mcp2515.reset();
  MCP2515::ERROR err1 = _mcp2515.setBitrate(_can_speed, _can_clock);

  if (err0 == err1 == MCP2515::ERROR_OK) {
    return MCP2515::ERROR_OK;
  } else {
    return MCP2515::ERROR_FAIL;
  }
}

/*
 * Allows user to set CAN operation mode. These are as follows:
 * Normal Mode: Allows for standard bidirectional communication between Motor and Arduino.
 * Loopback Mode: Arduino repeats every message it hears from Motor.
 * Listen Only Mode: Arduino can hear Motor commands, but cannot respond.
 */
MCP2515::ERROR RMDMotor::setCANMode(CAN_MODE can_mode) {
  if (can_mode == CAN_MODE::CAN_NORMAL) {
    _can_mode = CAN_MODE::CAN_NORMAL;
    return _mcp2515.setNormalMode();
  } else if (can_mode == CAN_MODE::CAN_LOOPBACK) {
    _can_mode = CAN_MODE::CAN_LOOPBACK;
    return _mcp2515.setLoopbackMode();
  } else if (can_mode == CAN_MODE::CAN_LISTEN_ONLY) {
    _can_mode = CAN_MODE::CAN_LISTEN_ONLY;
    return _mcp2515.setListenOnlyMode();
  } else {
    return MCP2515::ERROR_FAIL;
  }
}

CAN_MODE RMDMotor::getCANMode() {
  return _can_mode;
}

MCP2515::ERROR RMDMotor::getPID(struct motorPIDs *motorPIDs) {
  // Give user the current values
  motorPIDs->current_kp = _motorPIDs.current_kp;
  motorPIDs->current_ki = _motorPIDs.current_ki;
  motorPIDs->speed_kp = _motorPIDs.speed_kp;
  motorPIDs->speed_ki = _motorPIDs.speed_ki;
  motorPIDs->pos_kp = _motorPIDs.speed_kp;
  motorPIDs->pos_ki = _motorPIDs.speed_ki;

  // Ask the motor to send over the current version of these values, so that we can update the object.
  uint8_t data[8] = { 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

  return _sendMotorMessage(data);
}
/*
 * Sets PID parameters for various control loops. The temp boolean variable determines whether we permanently change
 * PID parameters by writing to ROM or temporarily change by adjusting volatile RAM.
 */
MCP2515::ERROR RMDMotor::setPID(struct motorPIDs *motorPIDs, bool temp) {
  // Set the internal object values
  _motorPIDs.current_kp = motorPIDs->current_kp;
  _motorPIDs.current_ki = motorPIDs->current_ki;
  _motorPIDs.speed_kp = motorPIDs->speed_kp;
  _motorPIDs.speed_ki = motorPIDs->speed_ki;
  _motorPIDs.pos_kp = motorPIDs->pos_kp;
  _motorPIDs.pos_ki = motorPIDs->pos_ki;
  // Tell the motor to change the PID values it is actually using
  uint8_t data[8];
  if (temp) {
    data[0] = 0x31;
  } else {
    data[0] = 0x32;
  }
  data[1] = 0x00;
  // Current control PID values
  data[2] = (uint8_t)round(255.0 * (motorPIDs->current_kp) / CURRENT_KP_MAX);
  data[3] = (uint8_t)round(255.0 * (motorPIDs->current_ki) / CURRENT_KI_MAX);
  // Speed control PID values
  data[4] = (uint8_t)round(255.0 * (motorPIDs->speed_kp) / SPEED_KP_MAX);
  data[5] = (uint8_t)round(255.0 * (motorPIDs->speed_ki) / SPEED_KI_MAX);
  // Position control PID values
  data[6] = (uint8_t)round(255.0 * (motorPIDs->pos_kp) / POS_KP_MAX);
  data[7] = (uint8_t)round(255.0 * (motorPIDs->pos_ki) / POS_KI_MAX);

  return _sendMotorMessage(data);
}

MCP2515::ERROR RMDMotor::getAcceleration(struct motorAcceleration *motorAccel) {
  motorAccel->pos_accel = _motorAcceleration.pos_accel;
  motorAccel->pos_decel = _motorAcceleration.pos_decel;
  motorAccel->spd_accel = _motorAcceleration.spd_accel;
  motorAccel->spd_decel = _motorAcceleration.spd_decel;
  return MCP2515::ERROR_OK;
}

MCP2515::ERROR RMDMotor::setAcceleration(struct motorAcceleration *motorAccel) {
  // set internal values
  _motorAcceleration.pos_accel = motorAccel->pos_accel;
  _motorAcceleration.pos_decel = motorAccel->pos_decel;
  _motorAcceleration.spd_accel = motorAccel->spd_accel;
  _motorAcceleration.spd_decel = motorAccel->spd_decel;

  /*
     * Write motor acceleration values. This takes four separate commands.
     */

  uint8_t data[8];
  // Position planning acceleration
  data[0] = 0x43;
  data[1] = 0x00;  // Position planning acceleration
  data[2] = 0x00;
  data[3] = 0x00;
  data[4] = (uint8_t) (_motorAcceleration.pos_accel);
  data[5] = (uint8_t) (_motorAcceleration.pos_accel >> 8);
  data[6] = (uint8_t) (_motorAcceleration.pos_accel >> 16);
  data[7] = (uint8_t) (_motorAcceleration.pos_accel >> 24);
  MCP2515::ERROR err0 = _sendMotorMessage(data);


  // Position planning deceleration
  data[0] = 0x43;
  data[1] = 0x01;  // Position planning deceleration
  data[2] = 0x00;
  data[3] = 0x00;
  data[4] = (uint8_t) (_motorAcceleration.pos_decel);
  data[5] = (uint8_t) (_motorAcceleration.pos_decel >> 8);
  data[6] = (uint8_t) (_motorAcceleration.pos_decel >> 16);
  data[7] = (uint8_t) (_motorAcceleration.pos_decel >> 24);
  MCP2515::ERROR err1 = _sendMotorMessage(data);

  // Speed planning acceleration
  data[0] = 0x43;
  data[1] = 0x02;  // Speed planning acceleration
  data[2] = 0x00;
  data[3] = 0x00;
  data[4] = (uint8_t) (_motorAcceleration.spd_accel);
  data[5] = (uint8_t) (_motorAcceleration.spd_accel >> 8);
  data[6] = (uint8_t) (_motorAcceleration.spd_accel >> 16);
  data[7] = (uint8_t) (_motorAcceleration.spd_accel >> 24);
  MCP2515::ERROR err2 = _sendMotorMessage(data);


  // Speed planning deceleration
  data[0] = 0x43;
  data[1] = 0x03;  // Speed planning deceleration
  data[2] = 0x00;
  data[3] = 0x00;
  data[4] = (uint8_t) (_motorAcceleration.spd_decel);
  data[5] = (uint8_t) (_motorAcceleration.spd_decel >> 8);
  data[6] = (uint8_t) (_motorAcceleration.spd_decel >> 16);
  data[7] = (uint8_t) (_motorAcceleration.spd_decel >> 24);
  MCP2515::ERROR err3 = _sendMotorMessage(data);

  // Now look at the error messages for each transmission and make sure everything went OK.
  if (err0 == err1 == err2 == err3 == MCP2515::ERROR_OK) {
    return MCP2515::ERROR_OK;
  } else {
    return MCP2515::ERROR_FAIL;
  }
}

MCP2515::ERROR RMDMotor::getOperatingMode(LOOP_MODE *loop_mode) {
  // Report the current value stored by the local object
  *loop_mode = _loop_mode;
  // Ask the motor for new info so that we can update the object
  uint8_t data[8] = { 0x70, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
  return _sendMotorMessage(data);
}

MCP2515::ERROR RMDMotor::getAngleMT(struct motorAngle *motorAngle) {
  // Give the user the most recent internal data
  motorAngle->angle = _motorAngle.angle;
  motorAngle->max_speed = _motorAngle.max_speed;

  // Ask the motor to send over the current version of these values, so that we can update the object.
  uint8_t data[8] = { 0x92, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

  return _sendMotorMessage(data);
}

MCP2515::ERROR RMDMotor::shutdown() {
  uint8_t data[8] = { 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
  return _sendMotorMessage(data);
}

MCP2515::ERROR RMDMotor::stop() {
  uint8_t data[8] = { 0x81, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
  return _sendMotorMessage(data);
}
/*
 * This method instructs the motor to go to a specific angle. Angle is defined using the MULTI-TURN method,
 * so it can be both positive and negative, and 360 is not the same as 0.
 */
MCP2515::ERROR RMDMotor::setAngleTargetMT(struct motorAngle *motorAngle) {
  uint8_t data[8];

  data[0] = 0xA4;
  data[1] = 0x00;
  data[2] = (uint8_t) (motorAngle->max_speed);
  data[3] = (uint8_t) (motorAngle->max_speed >> 8);

  int32_t scaledAngle = (int32_t)round(100 * motorAngle->angle);
  data[4] = (uint8_t) (scaledAngle);
  data[5] = (uint8_t) (scaledAngle >> 8);
  data[6] = (uint8_t) (scaledAngle >> 16);
  data[7] = (uint8_t) (scaledAngle >> 24);

  return _sendMotorMessage(data);
}
/*
 * This method instructs the motor to increase or decrease its position by a specific angular amount. Angle is again
 * defined using MULTI-TURN method, so the change direction is defined by positive/negative sign and the delta can be
 * greater than 360 degrees.
 */
MCP2515::ERROR RMDMotor::setAngleDeltaTargetMT(struct motorAngle *motorAngle) {
  uint8_t data[8];

  data[0] = 0xA8;
  data[1] = 0x00;
  data[2] = (uint8_t) (motorAngle->max_speed);
  data[3] = (uint8_t) (motorAngle->max_speed >> 8);

  int32_t scaledAngle = (int32_t)round(100 * motorAngle->angle);
  data[4] = (uint8_t) (scaledAngle);
  data[5] = (uint8_t) (scaledAngle >> 8);
  data[6] = (uint8_t) (scaledAngle >> 16);
  data[7] = (uint8_t) (scaledAngle >> 24);

  return _sendMotorMessage(data);
}
/*
 * This function resets everything, both the MCP2515 CAN device as well as the motor itself.
 */
MCP2515::ERROR RMDMotor::reset() {
  _mcp2515.reset();
  uint8_t data[8] = { 0x76, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
  return _sendMotorMessage(data);
}

MCP2515::ERROR RMDMotor::brakeRelease() {
  uint8_t data[8] = { 0x77, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
  return _sendMotorMessage(data);
}

MCP2515::ERROR RMDMotor::brakeLock() {
  uint8_t data[8] = { 0x78, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
  return _sendMotorMessage(data);
}

void RMDMotor::messageReceivedAlert() {
  _msg_received = true;
}

/*
 * This function checks both receive buffers to see what was received, in one or both.
 */
MCP2515::ERROR RMDMotor::_readMessages() {
  uint8_t irq = _mcp2515.getInterrupts();
  // Receive buffer 0 (RXB0)
  if (irq & MCP2515::CANINTF_RX0IF) {
    _readMotorMessage(MCP2515::RXB0);
  }
  // Receive buffer 1 (RXB1) -- not mutually exclusive with RXB0, hence lack of "else"
  if (irq & MCP2515::CANINTF_RX1IF) {
    _readMotorMessage(MCP2515::RXB1);
  }
}

/*
 * This helper function is used by the main readMessages function to get the data from RXB0
 * and RXB1 receive buffers. It reads the motor message, figures out what the message means
 * using the first command byte, and then updates the relevant internal parameter within the
 * RMDMotor object. If the message is useless or not one that the code recognizes, it is just
 * ignored.
 */
MCP2515::ERROR RMDMotor::_readMotorMessage(MCP2515::RXBn rxbn) {
  struct can_frame frame;

  // Try to receive 3 times. If third time doesn't work, return failure and exit function.
  int rx_attempts = 0;
  MCP2515::ERROR error = MCP2515::ERROR_FAIL;
  do {
    error = _mcp2515.readMessage(rxbn, &frame);
    rx_attempts++;
  } while (rx_attempts < 3 && error != MCP2515::ERROR_OK);
  if (error == MCP2515::ERROR_FAIL) {
    return error;
  }
  // DEBUGGING
  /*
  Serial.print("RX");
  Serial.print(rxbn);
  Serial.print(": ");
  _printCANFrameToSerial(&frame);
  */

  /*
     * The first byte of data always contains the motor command, which allows us to decipher the remaining bytes.
     */
  switch (frame.data[0]) {
    // GET PIDs
    case 0x30:
      _motorPIDs.current_kp = CURRENT_KP_MAX * (frame.data[2] / 255.0);
      _motorPIDs.current_ki = CURRENT_KI_MAX * (frame.data[3] / 255.0);
      _motorPIDs.speed_kp = SPEED_KP_MAX * (frame.data[4] / 255.0);
      _motorPIDs.speed_ki = SPEED_KI_MAX * (frame.data[5] / 255.0);
      _motorPIDs.pos_kp = POS_KP_MAX * (frame.data[6] / 255.0);
      _motorPIDs.pos_kp = POS_KI_MAX * (frame.data[7] / 255.0);
      break;
    // GET SHAFT ANGLE
    case 0x92:
      _motorAngle.angle = 0.01 * ((int32_t) ((frame.data[4]) | (frame.data[5] << 8) | (frame.data[6] << 16) | (frame.data[7] << 24)));
      break;
    // GET LOOP MODE. 0x01 is current, 0x02 is speed, 0x03 is position.
    case 0x70:
      switch (frame.data[7]) {
        case 0x01:
          _loop_mode = CURRENT_CONTROL;
          break;
        case 0x02:
          _loop_mode = SPEED_CONTROL;
          break;
        case 0x03:
          _loop_mode = POSITION_CONTROL;
          break;
        default:
          break;
      }
      break;
    /*
         * This default statement captures the following commands which don't return new/useful info:
         * 0x31, 0x32 (set PID)
         * 0x43 (set position planning acceleration)
         * 0x80, 0x81 (shutdown/stop)
         * 0xA4, 0xA8 (closed loop inputs) -- neat information but not relevant to us, since 0x92 info is more detailed
         * 0x76, 0x77, 0x78 (brake lock/release, system reset)
         */
    default:
      // Discard data -- do nothing.
      break;
  }

  return error;
}

/*
 * This function sends a message out to the motor using CAN protocol. MAJOR UPDATE: as opposed to before,
 * the function waits to hear a response back from the motor before declaring the send cycle to be complete.
 */
MCP2515::ERROR RMDMotor::_sendMotorMessage(uint8_t *data) {
  struct can_frame frame;
  frame.can_id = _can_id;
  frame.can_dlc = _can_dlc;
  for (int i = 0; i < _can_dlc; i++) {
    frame.data[i] = data[i];
  }
  // DEBUGGING
  /*
  Serial.print("TX: ");
  _printCANFrameToSerial(&frame);
  */

  // Try to transmit 3 times. If third time doesn't work, return the relevant failure.
  int tx_attempts = 0;
  MCP2515::ERROR tx_error = MCP2515::ERROR_FAIL;
  do {
    tx_error = _mcp2515.sendMessage(&frame);  // IDE doesn't like this -- I think false alarm, but good to note.
    tx_attempts++;
  } while (tx_attempts < 3 && tx_error != MCP2515::ERROR_OK);

  unsigned long start_time = millis();

  // The most we will ever wait to hear back from a command is 100ms
  while (!_msg_received && millis() < start_time + 100) {
    // idle
  }
  MCP2515::ERROR rx_error = MCP2515::ERROR_FAIL;
  
  // If the interrupt sets the message read flag to true, then fetch the message and interpret
  if (_msg_received) {
    rx_error = _readMessages();
  }

  // Set the flag false after this whole ordeal
  _msg_received = false;

  if(rx_error = tx_error = MCP2515::ERROR_OK){
    return MCP2515::ERROR_OK;
  } else {
    return MCP2515::ERROR_FAIL;
  }
}

/*
This method is used for debugging CAN transmissions. It prints the entirety of a CAN frame
to the serial monitor in a cogent, easy-to-understand way.
*/
void RMDMotor::_printCANFrameToSerial(struct can_frame *frame){
  Serial.print("[ID: 0x");
  Serial.print(frame->can_id, HEX);
  Serial.print("; DLC: 0x");
  Serial.print(frame->can_dlc, HEX);
  Serial.print("] --DATA--> [");
  for(int i=0; i<frame->can_dlc; i++){
    Serial.print("0x");
    Serial.print(frame->data[i], HEX);
    if(i != (frame->can_dlc - 1)){
      Serial.print(" | ");
    }
  }
  Serial.println("]\n");
}

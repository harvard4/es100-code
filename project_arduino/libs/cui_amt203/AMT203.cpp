#include "Arduino.h"
#include "AMT203.h"
#include "SPI.h"

AMT203::AMT203(int chip_select) {
    _cs = chip_select;
    _SPIn = &SPI;
}

void AMT203::begin() {
    pinMode(_cs, OUTPUT);
    digitalWrite(_cs, HIGH);
    _SPIn->begin();
}

/*
 * This device requires chip select to go back high after each 8-bit control sequence. I think
 * this has something to do with an internal bit counter, but the datasheet is not specific. I am
 * also pretty sure this is not standard SPI behavior (i.e. you should be able to send multiple commands
 * while CS stays low during the whole sequence) but what can you do?
 */
uint8_t AMT203::_sendCommand(uint8_t command) {
  uint8_t received = 0x00;
  digitalWrite(_cs, LOW);
  received = _SPIn->transfer(command);
  digitalWrite(_cs, HIGH);

  // DEBUG
  /*
  Serial.print("DATA [SEND, RECEIVE]: [");
  Serial.print(command, HEX);
  Serial.print(", ");
  Serial.print(received, HEX);
  Serial.println("]");
  */
  
  return received;
}

void AMT203::_openSPI() {

    _SPIn->beginTransaction(SPISettings(1000000, MSBFIRST, SPI_MODE0)); // This device only supports 1MHZ SPI.
    digitalWrite(_cs, LOW);
}

void AMT203::_closeSPI() {
    digitalWrite(_cs, HIGH);
    _SPIn->endTransaction();
}

// TODO: Ensure that angle actually increases going clockwise. Also add a maximum number of tx attempts.

/*
 * This function obtains the 12-bit angle measurement from the encoder, and converts
 * it to a decimal value. The encoder reports a single-turn angle value, which is converted
 * to a multi-turn angle ranging from (-180, 180]. Positive angle is CLOCKWISE (CW).
 */
double AMT203::getPosition() {
    _openSPI();

    uint8_t data_rx = 0;
    data_rx = _sendCommand(0x10);

    /*
     * We send the encoder NOP requests (0x00) and the encoder keeps responding WAIT (0xA5)
     * until we finally hear the originally sent command as a response (0x10). Once we hear
     * 0x10, we know the next two bytes will contain angle data.
     */
    while(data_rx != 0x10) {
        data_rx = _sendCommand(0x00);
    }
    // Bytes are received from high to low.
    uint16_t angle_raw = (_sendCommand(0x00)) << 8; // High byte
    angle_raw = angle_raw | (_sendCommand(0x00)); // Low byte

    _angle = 360.0 * (angle_raw / 4095.0);
    // Angles over 180 are reported as negative values, in accordance with multi-turn standard.
    if(_angle > 180){
        _angle = _angle - 360.0;
    }

    _closeSPI();
    return _angle;
}

void AMT203::setZero() {
    _openSPI();

    uint8_t data_rx;
    data_rx = _sendCommand(0x70);
    /*
     * We send the encoder NOP requests (0x00) and the encoder keeps responding WAIT (0xA5)
     * until we finally hear the 0x80 as a response. Once we hear 0x80, we know the zero
     * position has been set correctly. NB: device must be power cycled for new zero
     * position to take effect.
     */
    while(data_rx != 0x80) {
        data_rx = _sendCommand(0x00);
    }
    // If we made it here, then we received data_rx = 0x80 and we know zeroing was successful.
    _closeSPI();
}

#include "AMT203.h"

AMT203 amt203(10);

void setup(){
  Serial.begin(9600);
  amt203.begin();
  Serial.println("Initialization successful.");

  delay(100);
}

void loop(){
  double angle = amt203.getPosition();
  Serial.print("Sense Encoder Angle: ");
  Serial.println(angle);
  delay(1000);
}